*&---------------------------------------------------------------------*
*& Report z_invoice_items_euro
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_invoice_items_euro.

CLASS lcl_main DEFINITION CREATE PRIVATE.

  PUBLIC SECTION.
    CLASS-METHODS create
      RETURNING
        VALUE(ro_instance) TYPE REF TO lcl_main.

    METHODS run.

  PROTECTED SECTION.
  PRIVATE SECTION.

ENDCLASS.

CLASS lcl_main IMPLEMENTATION.


  METHOD create.
    ro_instance = NEW lcl_main( ).
  ENDMETHOD.


  METHOD run.
    WRITE: |Welcome { sy-uname }|, / |Today is the { sy-datum DATE = USER }|.
  ENDMETHOD.


ENDCLASS.

START-OF-SELECTION.
  lcl_main=>create( )->run( ).
