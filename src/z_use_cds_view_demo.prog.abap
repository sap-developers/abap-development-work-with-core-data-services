*&---------------------------------------------------------------------*
*& Report z_invoice_items_euro
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_use_cds_view_demo.

CLASS lcl_main DEFINITION CREATE PRIVATE.

  PUBLIC SECTION.
    CLASS-METHODS create
      RETURNING
        VALUE(ro_instance) TYPE REF TO lcl_main.

    METHODS run.

  PROTECTED SECTION.
  PRIVATE SECTION.

ENDCLASS.

CLASS lcl_main IMPLEMENTATION.


  METHOD create.
    ro_instance = NEW lcl_main( ).
  ENDMETHOD.


  METHOD run.

    cl_salv_gui_table_ida=>create_for_cds_view( 'Z_INVOICE_ITEMS_DEMO' )->fullscreen( )->display( ).

  ENDMETHOD.


ENDCLASS.

START-OF-SELECTION.
  lcl_main=>create( )->run( ).
