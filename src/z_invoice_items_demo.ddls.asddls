@AbapCatalog.sqlViewName: 'Z_ITEMS_DEMO'
@AbapCatalog.compiler.compareFilter: true
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Invoice Items'
define view Z_INVOICE_ITEMS_DEMO as select from sepm_sddl_so_invoice_item {

    header.buyer.company_name,
    sales_order_invoice_key,
    currency_code,
    gross_amount,
    
//    // @EndUserText.quickInfo: 'Paid'
//    case header.payment_status
//            when 'P' then 'X'
//            else ' '
//        end as payment_status,
        
    cast(
        case header.payment_status
            when 'P' then 'X'
            else ' '
        end as ZSO_INVOICE_PAYMENT_STATUS )
        as payment_status,
    
    //* Associations *//
    header
    
}

where currency_code = 'USD'